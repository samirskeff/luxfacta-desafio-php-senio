<?php
    class Poll
    {
        private $conn;
		private $table_name = "poll";

		// object properties
		public $poll_id;
		public $poll_description;
        public $poll_view;
        public $options;
		public $option_id;
        public $option_description;
        public $qty;

		// constructor with $db as database connection
	    public function __construct($db){
	        $this->conn = $db;
	    }

	    function readPoll(){ 
            // query to read single record
            $query = "SELECT
                        poll.poll_id, poll.poll_description, op.option_id, op.option_description
                    FROM
                        api_poll." . $this->table_name . " poll
                        inner join api_poll.poll_option as op 
                        	on op.poll_id = poll.poll_id
                    WHERE
                        poll.poll_id = :id ";

            // prepare query statement
            $stmt = $this->conn->prepare( $query );
         
            // bind id of atividade to be updated
            $stmt->bindParam(':id', $this->poll_id);         

            // execute query
            $stmt->execute();
         
            return $stmt;
        }

        function create(){
            $query = "INSERT INTO
                    api_poll." . $this->table_name . "
                SET
                    poll_description = :poll_description";
    
            // prepare query
            $stmt = $this->conn->prepare($query);
            // sanitize
            $this->poll_description=htmlspecialchars(strip_tags($this->poll_description));
            // bind new values
            $stmt->bindParam(':poll_description', $this->poll_description);
            $poll_id = '';
            if($stmt->execute()){
                $poll_id = $this->conn->lastInsertId();

                if(count($this->options)>0){
                    foreach ( $this->options as $options){
                        $option_description = $options->option_description;
                        $query = "INSERT INTO
                                    api_poll.poll_option
                                SET
                                    poll_id = :poll_id,
                                    option_description = :option_description";
                        $op = $this->conn->prepare($query);
                        // sanitize
                        $this->poll_id=htmlspecialchars(strip_tags($poll_id));
                        $this->option_description=htmlspecialchars(strip_tags($option_description));
                        // bind new values
                        $op->bindParam(':poll_id', $this->poll_id);
                        $op->bindParam(':option_description', $this->option_description);

                        $op->execute();            

                    }
                }
                
            }
            return $poll_id;
            
        }

        function votePoll(){ 
            // query to read single record
            $query = "SELECT
                        poll.poll_id, poll.poll_description, op.option_id, op.option_description, op.qty
                    FROM
                        api_poll." . $this->table_name . " poll
                        inner join api_poll.poll_option as op 
                            on op.poll_id = poll.poll_id
                    WHERE
                        op.option_id = :id ";

            // prepare query statement
            $stmt = $this->conn->prepare( $query );
         
            // bind id of atividade to be updated
            $stmt->bindParam(':id', $this->option_id);       

            // execute query
            if($stmt->execute()){
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $this->option_id = $row['option_id'];
                $this->qty = $row['qty']+1;

                // update query
                $query = "UPDATE
                            api_poll.poll_option
                        SET
                            qty = :qty
                            
                        WHERE
                            option_id = :option_id";
            
                // prepare query statement
                $stmt = $this->conn->prepare($query);

                $this->qty=htmlspecialchars(strip_tags($this->qty));
                $this->option_id=htmlspecialchars(strip_tags($this->option_id));

                $stmt->bindParam(':qty', $this->qty);
                $stmt->bindParam(':option_id', $this->option_id);
                
                if($stmt->execute()){
                
                    return $this->option_id;

                }   

            }else{
                $this->option_id = '';

                return $this->option_id;
            }

            return $stmt;
        }

        function addViewsPoll(){
            $query = "SELECT
                        poll.poll_id, poll.poll_description, poll.poll_view
                    FROM
                        api_poll." . $this->table_name . " poll
                    WHERE
                        poll.poll_id = :id 
                    ";

            // prepare query statement
            $stmt = $this->conn->prepare( $query );
         
            // bind id of atividade to be updated
            $stmt->bindParam(':id', $this->poll_id);         

            // execute query
             if($stmt->execute()){
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $this->poll_id = $row['poll_id'];
                $this->poll_view = $row['poll_view']+1;

                // update query
                $query = "UPDATE
                            api_poll.poll
                        SET
                            poll_view = :poll_view
                            
                        WHERE
                            poll_id = :poll_id";
            
                // prepare query statement
                $stmt = $this->conn->prepare($query);

                $this->poll_view=htmlspecialchars(strip_tags($this->poll_view));
                $this->poll_id=htmlspecialchars(strip_tags($this->poll_id));

                $stmt->bindParam(':poll_view', $this->poll_view);
                $stmt->bindParam(':poll_id', $this->poll_id);
                
                if($stmt->execute()){
                
                    return $this->poll_view;

                }   

            }
        }

        function statsPoll(){ 
            // query to read single record
            $query = "SELECT
                        op.option_id, op.option_description, op.qty
                    FROM
                        api_poll." . $this->table_name . " poll
                        inner join api_poll.poll_option as op 
                            on op.poll_id = poll.poll_id
                    WHERE
                        poll.poll_id = :id 
                    ORDER BY
                        op.qty DESC";

            // prepare query statement
            $stmt = $this->conn->prepare( $query );
         
            // bind id of atividade to be updated
            $stmt->bindParam(':id', $this->poll_id);         

            // execute query
            $stmt->execute();
         
            return $stmt;
        }

    }
?>