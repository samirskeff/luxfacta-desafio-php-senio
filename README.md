# luxfacta-desafio-php-senio

# objetivo
Projeto de referência a um **API Rest** em **PHP** contendo uma as soluções referente a solicitção do Desafio – PHP Sênio,  para empresa Luxfacta, referente a uma API de Enquete (Poll).

## Tecnologias utilizadas
- `Sistema Operacinal Linux Ubuntu 16.04`
- `Sevidor Web HTTP Apache 2`
- `Banco de Dados MySql 5.5`

## Configuração do Ambiente
- Apache 2

		 sudo apt-get update
		 sudo apt-get install apache2	
		 
		 sudo nano /etc/apache2/mods-enabled/dir.conf
		 
		 <IfModule mod_dir.c>
			DirectoryIndex index.php index.html index.cgi index.pl  index.xhtml index.htm
		</IfModule>
		 Move index.php para primeira posição e depois pressione CTRL+X, confirma Y e ENTER.
		 
		 sudo systemctl restart apache2
		 
		 sudo nano /etc/apache2/sites-available/000-default.conf
		 <VirtualHost *:80>
			...

			DocumentRoot /var/www/html

			<Directory /var/www/html>
				Options Indexes FollowSymLinks MultiViews
				AllowOverride All
				Order allow,deny
				Allow from all
			</Directory>

		</VirtualHost>

		sudo a2ensite 000-default.conf
		sudo systemctl restart apache2.service

		sudo a2enmod rewrite
		sudo service apache2 restart
	
- PHP 7.0
	
		 sudo add-apt-repository ppa:ondrej/php
		 sudo apt-get install php7.0
		 sudo apt-get install php7.0 php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-gd php7.0-mbstring
	
- Mysql 5.5
	
		 sudo apt-get update
		 sudo apt-get install mysql-server
		 mysql_secure_installation
		 mysqld --initialize
		 sudo systemctl start mysql
	     
	#### Obs.:
		Usuário: root , password: root
		
	Caso queria usar suas configuraç~oes so alterar no arquivo config/database.php
	
	Altere aqui:
	
		private $host = "localhost";
		private $db_name = "api_poll";
		private $username = "root";
		private $password = "root";
	

##Estrutura Banco de Dados MySql
- Estrutura Poll
		
		
		DROP TABLE IF EXISTS `poll`;

		CREATE TABLE `poll` (
		  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
		  `poll_description` varchar(155) NOT NULL,
		  `poll_view` int(11) DEFAULT NULL,
		  PRIMARY KEY (`poll_id`)
		) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
		
		INSERT INTO `poll` VALUES (1,'qual sua liguagem de programação favorita?',NULL)

- Estrutura Option Poll

		DROP TABLE IF EXISTS `poll_option`;


		CREATE TABLE `poll_option` (
		  `option_id` int(11) NOT NULL AUTO_INCREMENT,
		  `poll_id` int(11) NOT NULL,
		  `option_description` varchar(155) NOT NULL,
		  `qty` int(11) DEFAULT NULL,
		  PRIMARY KEY (`option_id`),
		  KEY `fk_pull_id_idx` (`poll_id`),
		  CONSTRAINT `fk_pull_id` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
		) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
		
		INSERT INTO `poll_option` VALUES (1,1,'Java',0),(2,1,'PHP',0),(3,1,'Phyton',0),(4,1,'Ruby',0)
		
## Endpoints
- Get	/poll/:id	

		GET http://<you-host>/<you-path>/poll/:id
	
	#### Exemplo
		GET http://localhost/luxfacta-desafio-php-senio/poll/1
		
		{
			"poll_id": "1",
			"poll_description": "qual sua liguagem de programação favorita?",
			"options": [
				{
					"option_id": "1",
					"option_description": "Java"
				},
				{
					"option_id": "2",
					"option_description": "PHP"
				},
				{
					"option_id": "3",
					"option_description": "Phyton"
				},
				{
					"option_id": "4",
					"option_description": "Ruby"
				}
			]
		}
			
		
- Post	/poll/

		POST http://<you-host>/<you-path>/poll/
	
	#### Exemplo
		POST http://localhost/luxfacta-desafio-php-senio/poll/
		No Postman vai em Body, clique em row e selecione JSON(application/json)
		Informe o codigo
		{
			"poll_description": "nova enquete teste",
			"options": [
				{
					"option_description": "option teste 1"
				},
				{
					"option_description": "option teste 2"
				},
				{
					"option_description": "option teste 3"
				}
			]
		}
		
		Resposta
		
		{
			"poll_id": "2"
		}
				
- Post	/poll/:id/vote

		POST http://<you-host>/<you-path>/poll/:id/vote
	
	#### Exemplo
		POST http://localhost/luxfacta-desafio-php-senio/poll/1/vote
		
		{
			"option_id": "1"
		}
			
- Get /poll/:id/stats

		GET http://<you-host>/<you-path>/poll/:id/stats
	
	#### Exemplo
		GET http://localhost/luxfacta-desafio-php-senio/poll/1/stats
		
		{
			"views": "1",
			"options": [
				{
					"option_id": "2",
					"qty": "11"
				},
				{
					"option_id": "3",
					"qty": "6"
				},
				{
					"option_id": "4",
					"qty": "4"
				},
				{
					"option_id": "1",
					"qty": "3"
				}
			]
		}		

