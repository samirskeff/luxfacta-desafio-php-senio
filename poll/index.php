<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../objects/poll.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare poll object
$poll = new Poll($db);

// get posted data
$body = json_decode(file_get_contents("php://input"));

$method = $_SERVER['REQUEST_METHOD'];


$id = 0;
$acao = '';

if(isset($_GET['id'])){
	$id = $_GET['id'];
}

if(isset($_GET['acao'])){
	$acao = $_GET['acao'];
}

//echo json_encode($method); die;

// Endpoint GET http://<you-host>/<you-path>/poll/:id
if($method === 'GET' and $acao == '' ){

	// set ID property of record to read
	$poll->poll_id = $id;
	// query atividades
	$stmt = $poll->readPoll();
	$num = $stmt->rowCount();


	if($num > 0){

    	$poll_id_aux = 0;

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    			extract($row);
				if($poll_id != $poll_id_aux){
					$poll_item=array(
		            "poll_id" => $poll_id,
		            "poll_description" => html_entity_decode($poll_description)
		        	);
				}
				$poll_options['options'][]=array(		            
		            "option_id" => $option_id,
		            "option_description" => html_entity_decode($option_description)		            
		        );

		        $poll_id_aux = $poll_id;
    		
    	}
		$poll_arr = array_merge($poll_item, $poll_options);

	    http_response_code(200);

	    // show products data in json format
	    echo json_encode($poll_arr);

	}else{
		// set response code - 404 Not found
	    http_response_code(404);
	 
	    // not exist
	    echo json_encode(array("message" => "404 Not Found"));
	}

}
// Endpoint POST http://<you-host>/<you-path>/poll/	
if( $method === 'POST' ){
	
	

	if($body->poll_description != ''){

		$poll->poll_description = $body->poll_description;

		$poll->options = $body->options;

		$poll_id = $poll->create();

		if($poll_id != ''){

	        // set response code - 201 created
	        http_response_code(201);
	 
	        // tell the user
	        echo json_encode(array("poll_id" => $poll_id ));
	    }else{

	    	http_response_code(404);
	 
		    // tell the user atividade does not exist
		    echo json_encode(array("message" => "404 Not Found"));

	    }

	}

}	

// Endpoint POST http://<you-host>/<you-path>/poll/:id/vote
if($method === 'POST' and $acao == "vote" ){

	// set ID property of record to read
	$poll->option_id = $id;
	// query atividades
	$vote = $poll->votePoll();
	//echo json_encode($id);
	//die;

	if($vote > 0){    	

	    http_response_code(200);

	    // show products data in json format
	    echo json_encode(array("option_id" => $vote ));

	}else{
		// set response code - 404 Not found
	    http_response_code(404);
	 
	    // not exist
	    echo json_encode(array("message" => "404 Not Found"));
	}

}

// Endpoint GET http://<you-host>/<you-path>/poll/:id/stats
if($method === 'GET' and $acao == "stats" ){

	// set ID property of record to read
	$poll->poll_id = $id;
	
	$views = $poll->addViewsPoll();

	
	$poll_views = array(
    	"views" => $views
	);

	$stats = $poll->statsPoll();
	$num = $stats->rowCount();


	if($num > 0){

		while ($row = $stats->fetch(PDO::FETCH_ASSOC)){
    			extract($row);
				
				$poll_options['options'][]=array(		            
		            "option_id" => $option_id,
		            "qty" => $qty
		        );
    		
    	}
		$poll_arr = array_merge($poll_views, $poll_options);

	    http_response_code(200);

	    // show products data in json format
	    echo json_encode($poll_arr);

	}else{
		// set response code - 404 Not found
	    http_response_code(404);
	 
	    // not exist
	    echo json_encode(array("message" => "404 Not Found"));
	}

}
	 
?>